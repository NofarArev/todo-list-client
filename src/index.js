import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import {TodoListApp} from "./components/TodoListApp";
import rootReducer from './reducers';
import {createStore} from 'redux';
import { Provider } from 'react-redux'

const store = createStore(rootReducer);

ReactDOM.render(
    <Provider store={store}>
        <TodoListApp/>
    </Provider>
    ,
    document.getElementById('root')
);
