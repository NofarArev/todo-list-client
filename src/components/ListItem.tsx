import React from "react";
import {FaTrash} from "react-icons/fa";
import {Task} from "../types/Task";
import {deleteTask, toggleTask, updateTask} from "../actions";
import {useDispatch} from "react-redux";

interface ListItem {
    task: Task;
}

export const ListItem = (props: ListItem) => {
    const dispatch = useDispatch();

    return (
        <div className='todo-item'>
            <div onClick={() => dispatch(toggleTask(props.task.id))}>
                {props.task.isCompleted ? (
                    <span className="todo-item-checked">✔</span>
                ) : (
                    <span className="todo-item-unchecked"/>
                )}
            </div>

            <div className="todo-item-input-wrapper">
                <input className={props.task.isCompleted ? "completed-task" : ""}
                       value={props.task.description}
                       onChange={(event: React.ChangeEvent<HTMLInputElement>) => dispatch(updateTask(event.target.value, props.task.id))}
                />
            </div>

            <div className="item-remove" onClick={() => dispatch(deleteTask(props.task.id))}>
                <FaTrash/>
            </div>
        </div>
    )
};