import React, {useRef, useState} from 'react';
import {FaPlusCircle} from "react-icons/fa";
import {addTask} from '../actions';
import {useDispatch} from "react-redux";

interface AddTaskProps {
}

export const AddTask = (props: AddTaskProps) => {
    const inputRef = useRef<HTMLInputElement>(null);
    const [taskDesc, setTaskDesc] = useState('');
    const dispatch = useDispatch();

    function handleInputChange(event: React.ChangeEvent<HTMLInputElement>) {
        setTaskDesc(event.target.value)
    }

    function handleInputEnter(event: React.KeyboardEvent) {
        // Check for 'Enter' key
        if (event.key === 'Enter') {
            dispatch(addTask(taskDesc));

            if (inputRef && inputRef.current) {
                inputRef.current.value = ''
            }
        }
    }

    return (
        <div className="input-container">
            <FaPlusCircle/>
            <input
                className="input-field"
                ref={inputRef}
                type="text"
                placeholder='Add task'
                onChange={event => handleInputChange(event)}
                onKeyPress={event => handleInputEnter(event)}
            />
        </div>
    )
};

