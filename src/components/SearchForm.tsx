import React, {useRef, useState} from 'react';
import {FaSearch} from "react-icons/fa";


interface SearchFormProps {
    handleFilteredTodos: (filter: string) => void;
}

export const SearchForm = (props: SearchFormProps) => {

    const inputRef = useRef<HTMLInputElement>(null);
    const [textFilter, setTextFilter] = useState('');

    function handleInputChange(event: React.ChangeEvent<HTMLInputElement>) {
        setTextFilter(event.target.value);
        props.handleFilteredTodos(event.target.value);
    }

    return (
        <div className="input-container">
            <FaSearch/>
            <input
                className="input-field"
                type="text"
                ref={inputRef}
                value={textFilter}
                onChange={event => handleInputChange(event)}
                placeholder="Serach.."
            />
        </div>
    );
};