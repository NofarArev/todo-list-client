import React, {useEffect, useState} from 'react';
import {TodoList} from "./TodoList";
import {SearchForm} from "./SearchForm";
import {FaSave} from "react-icons/fa";
import axios from 'axios';
import {Task} from "../types/Task";
import {AddTask} from "./AddTask";
import {getData} from "../actions";
import {useDispatch, useSelector} from "react-redux";
import {State} from "../reducers";

function useFilteredList(todos: Task[], filter: string) {
    const [filtered_todos, setFilteredTodos] = useState<Task[]>([]);

    useEffect(() => {
        const newFilteredList = todos.filter((task: Task) => task.description.toLowerCase().includes(filter.toLowerCase()));

        setFilteredTodos(newFilteredList);

    }, [todos, filter]);

    return filtered_todos;
}

export const TodoListApp = () => {
    const todos: Task[] = useSelector((state: State) => state.todos);
    const [filter, setFilter] = useState('');
    const filtered_todos = useFilteredList(todos, filter);
    const dispatch = useDispatch();

    function handleSaveClick() {
        saveData();
    }

    useEffect(() => {
        getData()(dispatch);
    }, []);

    function saveData() {
        axios.post(`http://localhost:3001/saveData`, JSON.stringify(todos), {
            headers: {
                'Access-Control-Allow-Origin': '*',
                'Content-Type': 'application/json'
            },
            params: JSON.stringify(todos),
            data: JSON.stringify(todos)
        })
            .then(res => {
                console.log(res);
                console.log(res.data);
                alert("saved successfully");
            }).catch(reason => {
            alert("there was a problem with the save. reason: " + reason)
        })
    }

    function handleFilteredTodos(filter: string) {
        setFilter(filter);
    }

    return (
        <div className="todo-list-app">
            <div>
                 <span onClick={handleSaveClick}>
                    <FaSave title="save" className="save-icon"/>
                </span>
                <h1>TODO list</h1>
            </div>

            <SearchForm handleFilteredTodos={handleFilteredTodos}/>

            <AddTask/>

            <TodoList
                todos={filtered_todos}
            />
        </div>
    )
};