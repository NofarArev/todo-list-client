import React from "react";
import {ListItem} from "./ListItem";
import {Task} from "../types/Task";

interface TodoListProps {
    todos: Task[],
}

export const TodoList = (props: TodoListProps) => {
    return (
        <div className="todo-list">
            <ul>
                {props.todos.map((task) => (
                    <li key={task.id}>
                        <ListItem
                            task={task}
                        />
                    </li>
                ))}
            </ul>
        </div>
    )
};