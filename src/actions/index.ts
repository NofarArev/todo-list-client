import {TodoListActions} from "../types/action-types";
import axios from 'axios';
import {Dispatch} from "redux";

let nextTaskId: number = 12;
export const addTask = (text: string): TodoListActions => ({
    type: 'ADD_TASK',
    task: {id: nextTaskId++, description: text, isCompleted: false}
});

export const toggleTask = (id: number): TodoListActions => ({
    type: 'TOGGLE_TASK',
    meta: {id: id}
});

export const updateTask = (text: string, id: number): TodoListActions => ({
    type: 'UPDATE_TASK',
    meta: {
        id: id,
        description: text
    }
});

export const deleteTask = (id: number): TodoListActions => ({
    type: 'DELETE_TASK',
    meta: {
        id: id
    }
});

export function getData() {
    return function (dispatch: Dispatch<TodoListActions>) {
        axios.get(`http://localhost:3001/getData`, {
            headers: {
                'Access-Control-Allow-Origin': '*',
                'Content-Type': 'application/json'
            }
        })
            .then(res => {
                dispatch({type: "DATA_LOADED", payload: JSON.stringify(res.data)});
            }).catch(reason => {
            alert("cant get data from server reason: " + reason);
        });
    }
}