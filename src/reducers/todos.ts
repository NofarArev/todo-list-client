import {Task} from "../types/Task";
import {TodoListActions} from "../types/action-types";

const todos = (state: Task[] = [], action: TodoListActions): Task[] => {
    switch (action.type) {
        case 'ADD_TASK':
            if (state.find(task => task.description === action.task.description)) {
                alert("task is already exists");
                return state;
            } else {
                return [
                    ...state,
                    {
                        id: action.task.id,
                        description: action.task.description,
                        isCompleted: false
                    }
                ];
            }
        case 'TOGGLE_TASK':
            return state.map(todo =>
                todo.id === action.meta.id ? {...todo, isCompleted: !todo.isCompleted} : todo
            );
        case 'UPDATE_TASK':
            return state.map(todo =>
                todo.id === action.meta.id ? {...todo, description: action.meta.description} : todo
            );
        case 'DELETE_TASK':
            return state.filter(todo => todo.id !== action.meta.id);
        case 'DATA_LOADED':
            return state = JSON.parse(action.payload);
        default:
            return state;
    }
};

// @ts-ignore
export default todos;

