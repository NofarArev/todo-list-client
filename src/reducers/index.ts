import { combineReducers } from 'redux';
import todos from './todos';
import {Task} from "../types/Task";

export type State = {
    todos: Task[]
}

export default combineReducers<State>({
    todos
});

