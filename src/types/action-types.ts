import {Task} from "./Task";

export const ADD_TASK = 'ADD_TASK';
export const DELETE_TASK = 'DELETE_TASK';
export const TOGGLE_TASK = 'TOGGLE_TASK';
export const UPDATE_TASK = 'UPDATE_TASK';
export const SET_FILTER = 'SET_FILTER';
export const DATA_LOADED = 'DATA_LOADED';

interface AddTaskAction {
    type: typeof ADD_TASK
    task: Task
}

interface UpdateTaskAction {
    type: typeof UPDATE_TASK
    meta: {
        id: number,
        description: string
    }
}

interface DeleteTaskAction {
    type: typeof DELETE_TASK
    meta: {
        id: number
    }
}

interface ToggleTaskAction {
    type: typeof TOGGLE_TASK
    meta: {
        id: number
    }
}

interface setFilter {
    type: typeof SET_FILTER
    meta: {
        todos: Task[],
        filter: string
    }
}

interface dataLoaded {
    type: typeof DATA_LOADED,
    payload: string
}

export type TodoListActions = AddTaskAction | DeleteTaskAction | ToggleTaskAction | UpdateTaskAction | setFilter | dataLoaded;
